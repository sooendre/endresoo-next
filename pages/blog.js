import Layout from "./../components/Layout";
import BlogListing from "../components/blog/BlogListing";
import withApollo from "../api/strapi/withApollo";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { getDataFromTree } from "@apollo/react-ssr";

const QUERY = gql`
  query {
    blogPosts {
      id
      slug
      category {
        Name
        slug
      }
      title
      created_at
      image {
        url
      }
    }
  }
`;

function Blog() {
  const { loading, data } = useQuery(QUERY);

  if (loading || !data) {
    return <h1>loading...</h1>;
  }

  return (
    <Layout>
      <BlogListing blogListing={data.blogPosts} />
    </Layout>
  );
}

// This gets called on every request
// export async function getServerSideProps() {
//   const { loading, data } = useQuery(QUERY);

//   if (loading || !data) {
//     return <h1>loading...</h1>;
//   }
//   return <h1>{data.title}</h1>;

//   for (let i = 0; i < blogListing.length; i++) {
//     blogListing[i].image = await firebase.getImage(blogListing[i].image);
//   }

//   console.log(data);

//   // Pass data to the page via props
//   return { props: { blogListing } };
// }

export default withApollo(Blog, { getDataFromTree });
