import Layout from "./../../../components/Layout";
import dynamic from "next/dynamic";
import "./../../../components/portfolio/countries/assets/countries_app.module.scss";

const DynamicCountriesApp = dynamic(
  () => import("./../../../components/portfolio/countries/CountriesApp"),
  {
    ssr: false,
  }
);

export default function Index() {
  return (
    <Layout>
      <section className="portfolio-item">
        <div className="neumorphism">
          <DynamicCountriesApp />
        </div>
      </section>
    </Layout>
  );
}
