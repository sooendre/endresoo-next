import Layout from "./../components/Layout";
import Home from "./../components/home/Home";
import withApollo from "../api/strapi/withApollo";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { getDataFromTree } from "@apollo/react-ssr";

const QUERY = gql`
  query {
    homePages {
      Hero {
        title
        subtitle
        hero_image {
          url
        }
      }
      title_image_text {
        Title
        Image {
          url
        }
        Text
      }
      list_images_text {
        List
        Images {
          url
        }
        Text
      }
      slide_screen {
        Body
        ID_pictures {
          url
        }
        explain_pictures {
          url
        }
      }
    }
  }
`;

export default withApollo(
  function Index() {
    const { loading, data } = useQuery(QUERY);

    if (loading || !data) {
      return <h1>loading...</h1>;
    }

    return (
      <Layout>
        <Home homePageData={data.homePages} />
      </Layout>
    );
  },
  { getDataFromTree }
);
