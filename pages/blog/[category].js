import Layout from "../../components/Layout";
import { default as BlogPostComponenet } from "../../components/blog/BlogPost";
import { useRouter } from "next/router";

import withApollo from "../../api/strapi/withApollo";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { getDataFromTree } from "@apollo/react-ssr";
import { from } from "apollo-boost";

function BlogPost() {
  const router = useRouter();

  if (!router.query.slug) {
    // return 404
  }

  const QUERY = gql`
    query {
        blogPosts(where: { slug: "${router.query.category}" }) {
            title
            body
            image {
                url
            }
            category {
                Name
            }
            created_at
            next_blog_post {
                title
                image{
                    url
                }
                slug
                category{
                    slug
                }
                created_at
            }
        }
    }
`;

  const { loading, data } = useQuery(QUERY);

  if (loading || !data) {
    return <h1>loading...</h1>;
  }

  //   console.log(data.blogPosts);

  const blogPosts = data.blogPosts.filter((post) => post.category === null);

  return (
    <Layout>
      <BlogPostComponenet {...data.blogPosts} />
    </Layout>
  );
}

export default withApollo(BlogPost, { getDataFromTree });
