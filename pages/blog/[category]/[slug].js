import Layout from "../../../components/Layout";
import { default as BlogPostComponenet } from "../../../components/blog/BlogPost";
import { useRouter } from "next/router";

import withApollo from "../../../api/strapi/withApollo";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { getDataFromTree } from "@apollo/react-ssr";

function BlogPost() {
  const router = useRouter();

  if (!router.query.slug) {
    // return 404
  }

  const QUERY = gql`
    query {
        blogPosts(where: { slug: "${router.query.slug}", category: { slug: "${
    router.query.category ? router.query.category : null
  }" } }) {
            title
            body
            image {
                url
            }
            category {
                Name
            }
            created_at
            next_blog_post {
                title
                image{
                    url
                }
                slug
                category{
                    slug
                }
                created_at
            }
        }
    }
`;

  const { loading, data } = useQuery(QUERY);

  if (loading || !data) {
    return <h1>loading...</h1>;
  }

  return (
    <Layout>
      <BlogPostComponenet {...data.blogPosts} />
    </Layout>
  );
}

export default withApollo(BlogPost, { getDataFromTree });
