import * as firebase from "firebase";

import "firebase/storage";

const prodConfig = {
  apiKey: process.env.REACT_APP_PROD_API_KEY,
  authDomain: process.env.REACT_APP_PROD_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_PROD_DATABASE_URL,
  projectId: process.env.REACT_APP_PROD_PROJECT_ID,
  storageBucket: process.env.REACT_APP_PROD_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_PROD_MESSAGING_SENDER_ID,
};
const devConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
};

const config =
  process.env.REACT_APP_NODE_ENV === "production" ? prodConfig : devConfig;

global.XMLHttpRequest = require("xhr2");

class Firebase {
  constructor() {
    if (!firebase.apps.length) firebase.initializeApp(config);
  }

  getBlogListing = () => {
    return new Promise((resolve, reject) => {
      firebase
        .database()
        .ref("/blog_listing")
        .once("value")
        .then((snapshot) => {
          resolve(snapshot.val());
        })
        .catch((e) => {
          // log error
          resolve([]);
        });
    });
  };

  getBlogPost = (slug) => {
    return new Promise((resolve, reject) => {
      firebase
        .database()
        .ref(`/blog/${slug}`)
        .once("value")
        .then((snapshot) => {
          resolve(snapshot.val());
        })
        .catch((e) => {
          // log error
          resolve([]);
        });
    });
  };

  getImage = (ref) => {
    return new Promise((resolve, reject) => {
      firebase
        .storage()
        .ref(ref)
        .getDownloadURL()
        .then((url) => {
          resolve(url);
        })
        .catch((error) => {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
            case "storage/object-not-found":
              // File doesn't exist
              break;

            case "storage/unauthorized":
              // User doesn't have permission to access the object
              break;
            case "storage/canceled":
              // User canceled the upload
              break;
            case "storage/unknown":
              // Unknown error occurred, inspect the server response
              break;
          }
          reject("default/generic/gb/image");
        });
    });
  };
}

export default Firebase;
