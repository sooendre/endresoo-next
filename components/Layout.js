import Menu from "./navigation/Menu";
import Footer from "./navigation/Footer";

export default function Layout(props) {
  return (
    <React.Fragment>
      <Menu />
      {props.children}
      {/* <Footer /> // commented out until design */}
    </React.Fragment>
  );
}
