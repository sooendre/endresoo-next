import Link from "next/link";

export default function AppLink(props) {
  const { title, href, className } = props;
  return (
    <Link href={href}>
      <a title={title} className={className}>
        {title}
      </a>
    </Link>
  );
}
