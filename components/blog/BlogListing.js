import ListItem from "./ListItem";
import { ParallaxProvider } from "react-scroll-parallax";

export default function BlogListing(props) {
  const { blogListing } = props;

  return (
    <ParallaxProvider>
      <div className="blog-listing">
        {/* Title */}
        <h1>Blog</h1>
        <main>
          {blogListing.map((itemData, index) => (
            <ListItem itemData={itemData} key={itemData.id} index={index} />
          ))}
        </main>
      </div>
    </ParallaxProvider>
  );
}
