import NextPost from "./NextPost";
import { useState } from "react";

export default function BlogPost(props) {
  if (!props[0]) {
    return <h1>loading...</h1>;
  }

  const [animate, setAnimate] = useState("");

  setTimeout(() => {
    setAnimate("animate");
  }, 300);

  const { body, category, image, title, created_at, next_blog_post } = props[0];

  // change date format
  const createdFormat = new Date(created_at);

  return (
    <main className="blog-page">
      <div
        className="hero-wrapper"
        style={{ backgroundImage: `url('${image.url}')` }}
      >
        <div className="centered-text-container">
          <h1>{title}</h1>
          <div className={`sub-title ${animate}`}>
            <div className="date">
              {String(createdFormat.getDate()).padStart(2, "0") +
                " / " +
                String(createdFormat.getMonth() + 1).padStart(2, "0") +
                " / " +
                createdFormat.getFullYear()}
            </div>
            <div className="category">{category && category.Name}</div>
          </div>
        </div>
      </div>
      <div className={`body ${animate}`}>{body}</div>
      {next_blog_post && <NextPost {...next_blog_post} />}
    </main>
  );
}
