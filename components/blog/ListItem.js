import Link from "next/link";
import { Parallax } from "react-scroll-parallax";
import { Fragment } from "react";

export default function ListItem(props) {
  const { category, created_at, title, image, slug } = props.itemData;
  const { index } = props;
  const reverse = index % 2 === 1;

  const formatDate = (date) => {
    let jsDate = new Date(date);
    return (
      jsDate.getDay() + "/" + jsDate.getMonth() + "/" + jsDate.getFullYear()
    );
  };

  const dateFormatted = formatDate(created_at);

  const imageContainer = image !== null && (
    <div className="image-container">
      <Parallax y={[`-${(index + 1) * 30 + 20}px`, "50px"]}>
        <img src={image.url} />
      </Parallax>
    </div>
  );

  return (
    <Fragment>
      <article className={`blog-post${reverse ? " reverse" : ""}`}>
        {reverse && imageContainer}
        <div className="text-container">
          <Parallax y={[`${index * 80}px`, "-80px"]}>
            <div className="date">{dateFormatted}</div>
            <div className="category">{category && category.Name}</div>
            <Link href={`/blog${category ? "/" + category.slug : ""}/${slug}`}>
              <a title={title}>
                <h2>{title}</h2>
              </a>
            </Link>
          </Parallax>
        </div>

        {!reverse && imageContainer}
      </article>
      <div className="spacer" y={[`-${index * 30}px`, "30px"]}></div>
    </Fragment>
  );
}
