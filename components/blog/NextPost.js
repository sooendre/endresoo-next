import Link from "next/link";
import { useState } from "react";

export default function BlogPost(props) {
  const [clicked, setClicked] = useState(false);

  const { category, image, title, created_at, slug } = props;

  // change date format
  const createdFormat = new Date(created_at);

  const href = `/blog${category ? "/" + category.slug : ""}/${slug}`;

  return (
    <div className={`next-post ${clicked ? " clicked" : ""}`}>
      <Link href={href}>
        <a
          title={title}
          onClick={(e) => {
            console.log(e);
            e.preventDefault();
            setClicked(true);
            setTimeout(() => {
              window.location = href;
            }, 600);
          }}
        >
          <section
            className="full-width-image"
            style={{
              backgroundImage: `url('${image.url}')`,
            }}
          >
            <h3>{title}</h3>
          </section>
        </a>
      </Link>
    </div>
  );
}
