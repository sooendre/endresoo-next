import AppLink from "../general/AppLink";

export default function Footer() {
  return (
    <footer>
      <div className="footer-links">
        <div>
          <AppLink title="Sitemap" href="#" />
        </div>
        <div>
          <AppLink title="Cookies" href="#" />
        </div>
        <div>
          <AppLink title="Terms & conditions" href="#" />
        </div>
        <div>
          <AppLink title="Contact" href="#" />
        </div>
      </div>

      <div className="footer-links">
        <div>
          <a title="sooendre@gmail.com" href="mailto:sooendre@gmail.com">
            sooendre@gmail.com
          </a>
        </div>
        <div>
          <a title="" href="#" className="linkedin">
            linkedin
          </a>
        </div>
        <div>
          <a title="" href="#" className="twitter">
            twitter
          </a>
        </div>
        <div>
          <a title="" href="#" className="facebook">
            facebook
          </a>
        </div>
        <div>
          <a title="" href="#" className="instagram">
            instagram
          </a>
        </div>
      </div>
    </footer>
  );
}
