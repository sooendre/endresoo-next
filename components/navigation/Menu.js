import Link from "next/link";
// import { UserContext } from "./../auth/UserContext";
// import Cookies from "js-cookie";
// import Auth from "./../../api/Auth";

export default function Menu() {
  const [open, setOpen] = React.useState(false);
  // const [auth, setAuth] = React.useState(false);
  // const [user, setUser] = React.useContext(UserContext);

  // const fetchData = async () => {
  //   try {
  //     if (Cookies.get("auth")) {
  //       const auth = new Auth();
  //       const result = await auth.getUser(Cookies.get("auth"));
  //       const res = await result.json();
  //       return res;
  //     }
  //   } catch (error) {
  //     throw error;
  //   }
  // };

  // React.useEffect(() => {
  //   fetchData()
  //     .then(res => {
  //       setUser({ user: res });
  //       setAuth(true);
  //     })
  //     .catch(error => {
  //       // TODO: handle error
  //       console.warn(JSON.stringify(error, null, 2));
  //     });
  // }, [auth]);

  const toggleMEnu = () => {
    setOpen(!open);
  };

  const handleClick = () => {
    setOpen(false);
  };

  // let userContextLinks = (
  //   <React.Fragment>
  //     <li>
  //       <Link
  //         to="/register"
  //         title="Register"
  //         data-text={"Register"}
  //         onClick={handleClick}
  //       >
  //         Register
  //       </Link>
  //     </li>
  //     <li>
  //       <Link
  //         to="/login"
  //         title="Login"
  //         data-text={"Login"}
  //         onClick={handleClick}
  //       >
  //         Login
  //       </Link>
  //     </li>
  //   </React.Fragment>
  // );

  // if (user.user) {
  //   userContextLinks = (
  //     <React.Fragment>
  //       <li>
  //         <Link
  //           title="Album"
  //           data-text={"Album"}
  //           to="/album"
  //           onClick={handleClick}
  //         >
  //           Album
  //         </Link>
  //       </li>
  //       <li>
  //         <Link
  //           title="Login"
  //           data-text={"Login"}
  //           to="#"
  //           onClick={() => {
  //             Cookies.remove("auth");
  //             setUser({ user: null });
  //             handleClick();
  //           }}
  //         >
  //           Logout
  //         </Link>
  //       </li>
  //     </React.Fragment>
  //   );
  // }

  return (
    <React.Fragment>
      <div onClick={toggleMEnu} className={`menu-button ${open && "open"}`}>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <nav className={`${open && "open"} main-navigation`}>
        <ul className={"menu"}>
          <li>
            <Link href="/">
              <a title="Home" data-text={"Home"}>
                Home
              </a>
            </Link>
          </li>
          <li>
            <Link href="/blog">
              <a title="Blog" data-text={"Blog"}>
                Blog
              </a>
            </Link>
          </li>
          {/* {userContextLinks} */}
        </ul>
      </nav>
    </React.Fragment>
  );
}
