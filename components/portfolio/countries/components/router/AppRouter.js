import React from "react";
import { Router } from "@reach/router";
import Home from "../home/Home";
import Country from "../country/Country";
import { BasePath } from "./../../CountriesApp";

const AppRouter = () => (
  <Router>
    <Home path={`${BasePath}`} />
    <Country path={`${BasePath}/country/:id`} />
  </Router>
);

export default AppRouter;
