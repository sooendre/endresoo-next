import React, { useContext } from "react";
import ThemeContext from "./../../contexts/ThemeContext";

export default function Mode() {
  const [theme, setTheme] = useContext(ThemeContext);

  return (
    <button
      className="mode-selector"
      name="mode_selector"
      onClick={() => {
        setTheme(theme == "dark" ? "light" : "dark");
      }}
    >
      {theme == "dark" ? "light" : "dark"} mode
    </button>
  );
}
