import React, { useContext } from "react";
import Mode from "./Mode";
import ThemeContext from "./../../contexts/ThemeContext";

export default function Nav() {
  const [theme] = useContext(ThemeContext);

  return (
    <nav className={theme}>
      <h1>Where in the world?</h1>
      <Mode />
    </nav>
  );
}
