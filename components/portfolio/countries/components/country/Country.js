import { useState, useEffect, useContext } from "react";
import { Link, useParams } from "@reach/router";
import LoadingAnimation from "../LoadingAnimation";
import { BasePath } from "./../../CountriesApp";
import ThemeContext from "./../../contexts/ThemeContext";

export default function Country() {
  const { id } = useParams();
  const [country, setCountry] = useState({});
  const [borderCountries, setBorderCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [bordersLoading, setBordersLoading] = useState(true);
  const [error, setError] = useState(false);

  const [theme] = useContext(ThemeContext);

  useEffect(() => {
    typeof id === "string" &&
      id !== "" &&
      fetch(`https://restcountries.eu/rest/v2/alpha?codes=${id}`)
        .then((res) => res.json())
        .then((data) => {
          setCountry(data[0]);
          setLoading(false);
        })
        .catch(() => {
          setLoading(false);
          setError(true);
        });

    country &&
      country.borders &&
      country.borders.length &&
      fetch(
        `https://restcountries.eu/rest/v2/alpha?codes=${country.borders.join(
          ";"
        )}&fields=name;alpha2Code`
      )
        .then((res) => res.json())
        .then((data) => {
          setBorderCountries(data);
          setBordersLoading(false);
        })
        .catch(() => {
          setBordersLoading(false);
          setError(true);
        });
  }, [loading, bordersLoading]);

  return (
    <main className={`country-wrapper ${theme}`}>
      <div className={`back-container ${theme}`}>
        <Link to={`${BasePath}`}> Back</Link>
      </div>
      {loading ? (
        <LoadingAnimation />
      ) : (
        <div className={`country-container ${theme}`}>
          <img src={country.flag} alt={`Flag of ${country.name}`} />
          <div className="country-data">
            <h3>{country.name}</h3>

            <div className="country-detail">
              <p>
                <strong>Native Name: </strong>
                {country.nativeName}
              </p>
              <p>
                <strong>Population: </strong>
                {country.population
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
              </p>
              <p>
                <strong>Region: </strong>
                {country.region}
              </p>
              <p>
                <strong>Sub Region: </strong>
                {country.subregion}
              </p>
              <p>
                <strong>Capital: </strong>
                {country.capital}
              </p>
            </div>

            <div className="country-detail">
              <p>
                <strong>Top Level Domain: </strong>
                <span>
                  {country.topLevelDomain.map((domain) => domain + " ")}
                </span>
              </p>
              <p>
                <strong>Currencies: </strong>
                <span>
                  {country.currencies.map((currency) => currency + " ")}
                </span>
              </p>
              <p>
                <strong>Languages: </strong>
                {country.region}
              </p>
            </div>

            <div className="border-countries-container">
              <div className="title">
                <span>Border Countries</span>
              </div>
              <div className="border-countries">
                {borderCountries ? (
                  borderCountries.length ? (
                    borderCountries.map((country) => (
                      <span key={country.alpha2Code}>{country.name}</span>
                    ))
                  ) : (
                    <span>No border countries</span>
                  )
                ) : (
                  <LoadingAnimation />
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </main>
  );
}
