import React, { useContext, useState } from "react";
import FilterContext from "./../../contexts/FilterContext";

export default function Filter() {
  const [filter, setFilter] = useContext(FilterContext);
  const [region, setRegion] = useState(filter);

  return (
    <div className="filter-container">
      <select
        id="filter"
        name="filter"
        value={filter}
        onChange={(e) => {
          setFilter(e.target.value);
          setRegion(filter);
        }}
        onBlur={(e) => {
          setFilter(e.target.value);
          setRegion(filter);
        }}
      >
        <option value="">All</option>
        <option value="africa">Africa</option>
        <option value="americas">Americas</option>
        <option value="asia">Asia</option>
        <option value="europe">Europe</option>
        <option value="oceania">Oceania</option>
      </select>
    </div>
  );
}
