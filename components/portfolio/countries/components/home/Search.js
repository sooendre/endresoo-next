import { useContext } from "react";
import SearchContext from "./../../contexts/SearchContext";

export default function Search() {
  const [search, setSearch] = useContext(SearchContext);

  return (
    <div className="search-container">
      <input
        id="search"
        name="search"
        placeholder="Search for a country"
        onChange={(e) => setSearch(e.target.value)}
      />
    </div>
  );
}
