import React, { useState, useEffect, useContext } from "react";
import ListItem from "./ListItem";
import LoadingAnimation from "../LoadingAnimation";
import FilterContext from "./../../contexts/FilterContext";
import SearchContext from "./../../contexts/SearchContext";

export default function List() {
  const [filter, setFilter] = useContext(FilterContext);
  const [search] = useContext(SearchContext);

  const [countries, setCountries] = useState([]);
  const [filteredCountries, setFilteredCountries] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(
      `https://restcountries.eu/rest/v2/${
        filter ? "region/" + filter : "all"
      }?fields=name;population;region;capital;flag;alpha2Code`
    )
      .then((res) => res.json())
      .then((data) => {
        setCountries(data);
        setFilteredCountries(data);
        setLoading(false);
      })
      .catch(() => {
        //   catch error
      });
  }, [filter]);

  useEffect(() => {
    if (search && search !== "")
      setFilteredCountries(
        countries.filter((country) => {
          return country.name.toLowerCase().includes(search.toLowerCase());
        })
      );
    else setFilteredCountries(countries);
  }, [search]);

  return (
    <div className="list-wrapper">
      {loading ? (
        <LoadingAnimation />
      ) : (
        filteredCountries.map((country) => (
          <ListItem key={country.alpha2Code} country={country} />
        ))
      )}
    </div>
  );
}
