import React from "react";
import { Link } from "@reach/router";
import { BasePath } from "./../../CountriesApp";

export default function ListItem(props) {
  const { name, population, region, capital, flag, alpha2Code } = props.country
    ? props.country
    : false;
  return (
    <Link to={`${BasePath}/country/${alpha2Code}`} className="list-item">
      <img src={flag} alt={`Flag of ${name}`} />
      <div className="item-text">
        <h3>{name}</h3>
        <p>
          <strong>Population: </strong>
          {population &&
            population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
        </p>
        <p>
          <strong>Region: </strong>
          {region}
        </p>
        <p>
          <strong>Capital: </strong>
          {capital}
        </p>
      </div>
    </Link>
  );
}
