import React, { useState, useContext } from "react";
import Search from "./Search";
import Filter from "./Filter";
import List from "./List";
import FilterContext from "./../../contexts/FilterContext";
import ThemeContext from "./../../contexts/ThemeContext";

export default function Home() {
  const region = useState("europe");

  const [theme] = useContext(ThemeContext);
  return (
    <FilterContext.Provider value={region}>
      <main className={theme}>
        <div className="search-filter">
          <Search className={theme} />
          <Filter className={theme} />
        </div>
        <List className={theme} />
      </main>
    </FilterContext.Provider>
  );
}
