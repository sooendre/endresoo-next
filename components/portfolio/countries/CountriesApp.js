import React, { useState } from "react";

import Nav from "./components/navigation/Nav";
import AppRouter from "./components/router/AppRouter";
import ThemeContext from "./contexts/ThemeContext";
import SearchContext from "./contexts/SearchContext";

export default function App() {
  const theme = useState("dark");
  const search = useState("");

  return (
    <ThemeContext.Provider value={theme}>
      <SearchContext.Provider value={search}>
        <Nav />
        <AppRouter />
      </SearchContext.Provider>
    </ThemeContext.Provider>
  );
}

export const BasePath = "/portfolio/countries";
