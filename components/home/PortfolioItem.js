import ReactMarkdown from "react-markdown";

export default function PortfolioItem(props) {
  const { Body, ID_pictures, explain_pictures } = props.data;

  const techHTML = ID_pictures.map((image) => (
    <div className="image-wrapper">
      <img src={image.url} />
    </div>
  ));

  return (
    <div className="portfolio-item">
      <div className="black-rect-effect"></div>

      <div className="project-trivia">
        <div className="technology-images">{techHTML}</div>

        <div className="text">
          <ReactMarkdown source={Body} />
        </div>
      </div>

      <div className="project-decoration">
        {explain_pictures[0] && (
          <div className="large-image">
            <img src={explain_pictures[0].url} alt="" title="" />
          </div>
        )}

        {explain_pictures[1] && (
          <div className="small-image">
            <img src={explain_pictures[1].url} alt="" title="" />
          </div>
        )}
      </div>
    </div>
  );
}
