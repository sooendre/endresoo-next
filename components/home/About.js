import ParallaxBackgroundText from "./ParallaxBackgroundText";
import { Fragment } from "react";
import ReactMarkdown from "react-markdown";

export default function About(props) {
  const { Title, Image, Text } = props.data;
  // title TODO: export to a function
  const titleParts = Title.split(" ");
  const titleBrs = titleParts.length - 1;
  const titleHTML = titleParts.map((part, index) => {
    return (
      <Fragment key={part + "_" + index}>
        {part} {index < titleBrs && <br />}
      </Fragment>
    );
  });

  return (
    <div className="about-us">
      <div
        className="background-container"
        style={{
          backgroundImage: `url('${Image.url}')`,
        }}
      >
        <ParallaxBackgroundText text="Who am I?" />

        <div className="black-rect"></div>

        <div className="white-rect"></div>
      </div>

      <div className="content-wrapper">
        <h3>{titleHTML}</h3>
        <div className="white-text-wrapper">
          <ReactMarkdown source={Text} />
        </div>
      </div>
    </div>
  );
}
