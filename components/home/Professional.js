import ParallaxBackgroundText from "./ParallaxBackgroundText";
import ReactMarkdown from "react-markdown";

export default function Professional(props) {
  const { List, Images, Text } = props.data;

  return (
    <div className="professional">
      <div className="background-container">
        <ParallaxBackgroundText text="What do I do?" />
        <div className="black-rect"></div>
      </div>

      <div className="content-wrapper">
        <div className="white-text-wrapper technologies">
          <ReactMarkdown source={List} />
        </div>

        <div className="image-decor">
          {Images.length &&
            Images.map((image) => {
              return <img key={image.url} src={image.url} alt="" title="" />;
            })}
        </div>

        <div className="red-text-wrapper">
          <ReactMarkdown source={Text} />
        </div>
      </div>
    </div>
  );
}
