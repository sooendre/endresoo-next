export default function WelcomeHero(props) {
  const { title, subtitle, hero_image } = props.data;
  return (
    <div
      className="hero-wrapper"
      style={{
        backgroundImage: `url('${hero_image.url}')`,
      }}
    >
      <div className="centered-text-container">
        <h1>{title}</h1>
        <h2>{subtitle}</h2>
      </div>
    </div>
  );
}
