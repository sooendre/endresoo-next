import PortfolioItem from "./PortfolioItem";
import ParallaxBackgroundText from "./ParallaxBackgroundText";

export default function portfolio(props) {
  const slideScreens = props.data;

  return (
    <div className="portfolio">
      <h3>
        <ParallaxBackgroundText text="My latest work" />
      </h3>

      {slideScreens.length &&
        slideScreens.map((item) => <PortfolioItem data={item} />)}
    </div>
  );
}
