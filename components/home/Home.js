import WelcomeHero from "./WelcomeHero";
import About from "./About";
import Professional from "./Professional";
import Portfolio from "./Protfolio";

export default function Home(props) {
  const {
    Hero,
    title_image_text,
    list_images_text,
    slide_screen,
  } = props.homePageData[0];

  return (
    <div className="home-page">
      <WelcomeHero data={Hero} />
      {/* Hero */}
      <main>
        <About data={title_image_text} />
        <Professional data={list_images_text} />
        <Portfolio data={slide_screen} />
      </main>
    </div>
  );
}
