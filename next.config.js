// next.config.js
const withSass = require("@zeit/next-sass");
require("dotenv").config();

module.exports = withSass({
  /* config options here */
  exportPathMap: async function () {
    const paths = {
      "/": { page: "/" },
      "/portfolio/countries": { page: "/portfolio/countries" },
    };

    return paths;
  },
  env: {
    STRAPI_API_URL: process.env.STRAPI_API_URL,
  },
});
